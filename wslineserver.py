import asyncio
import datetime
import random
import websockets
import sys

messages = []
connected = set()

async def async_readio (loop):
    while True:
        line = await loop.run_in_executor(None, sys.stdin.readline)
        print ("{0}: {1}".format(len(connected), line))
        if connected:
            await asyncio.wait([ws.send(line) for ws in connected])
            messages.append(line)

async def connection_handler(websocket, path):
    global connected
    connected.add(websocket)
    try:
        for msg in messages:
            await websocket.send(msg)
        while True:
            message = await websocket.recv()
            # consumer(message)
    finally:
        connected.remove(websocket)
        
#async def time(websocket, path):
#    while True:
#        now = datetime.datetime.utcnow().isoformat()+"Z"
#        await websocket.send(now)
#        await asyncio.sleep(random.random()*3)

start_server = websockets.serve(connection_handler, '0.0.0.0', 5678)

loop = asyncio.get_event_loop()
loop.run_until_complete(start_server)
loop.run_until_complete(async_readio(loop))
asyncio.get_event_loop().run_forever()
