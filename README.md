Files for presentation at Recyclart Dec 2017.

Constant_V is a series of installations that usually take place in the window of Constant, rue du Fort 5, in Saint-Gilles.
Constant_V prefers showing work in progress, highlighting the conceptual, technical and collaborative processes part of creating an artwork. The installations are made with Free Software and / or distributed under open content licenses. This way Constant_V offers passers a glimpse into the world of F/LOSS arts.
On Tuesday, December 12th, Constant_V goes to Recyclart. It is not a literal move but an opportunity for different artists to show how their work and artistic research has developed in the mean time.
Expect an (inter)active multi-voice intervention!

On the agenda:

    Antje Van Wichelen, 21c/19c. Procedures for Anthropometric Image Reversal
    Claire Williams, An electromagnetic walk
    Katrien Oosterlinck, Espace autour
    Michael Murtaugh, Nicolas Malevé & Ellef Prestsæter SICV, Scandinavian Institute for Computational Vandalism
    Pascale Barret, Ce côté obscur de la douceur attend de fleurir
