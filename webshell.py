#!/usr/bin/env python3
"""Example for aiohttp.web websocket server
"""

import asyncio
import os
from asyncio.subprocess import PIPE, STDOUT
from aiohttp.web import (Application, Response, WebSocketResponse, WSMsgType,
                         run_app)


WS_FILE = os.path.join(os.path.dirname(__file__), 'webshell.html')

async def read_stream_and_send_messages(stream, messagetype, sockets):
    """Read from stream line by line until EOF, display, and capture the lines.

    """
    outputcount = 0
    while True:
        # LOG.info("read_stream_and_display.readline")
        line = await stream.readline()
        if not line:
            break
        line = line.decode("utf-8")
        for ws in sockets:
            await ws.send_str(messagetype+": "+line)
        outputcount += 1
        # display(line) # assume it doesn't block
    #return b''.join(output)
    return outputcount

async def run_process (cmd, sockets):
    # process = await asyncio.create_subprocess_shell(cmd, stdout=PIPE, stderr=STDOUT, stdin=None)
    cmd = cmd.split()
    process = await asyncio.create_subprocess_exec(*cmd, stdout=PIPE, stderr=PIPE, stdin=None)
    print ("process", process)

    try:
        stdout, stderr = await asyncio.gather(
            read_stream_and_send_messages(process.stdout, "stdout", sockets),
            read_stream_and_send_messages(process.stderr, "stderr", sockets))
        return stdout, stderr
    except Exception:
        process.kill()
        raise
    finally:
        # wait for the process to exit
        rc = await process.wait()
        print ("process ended (normally)")
async def wshandler(request):
    resp = WebSocketResponse()
    ok, protocol = resp.can_prepare(request)
    if not ok:
        with open(WS_FILE, 'rb') as fp:
            return Response(body=fp.read(), content_type='text/html')

    await resp.prepare(request)

    try:
        print('Someone joined.')
        for ws in request.app['sockets']:
            await ws.send_str('Someone joined')
        request.app['sockets'].append(resp)

        async for msg in resp:
            if msg.type == WSMsgType.TEXT:
                # PERFORM SHELL
                print ("run_process", msg.data)
                await run_process(msg.data, request.app['sockets'])
                # old chat code: send to other clients
                # for ws in request.app['sockets']:
                #     if ws is not resp:
                #         await ws.send_str(msg.data)
            else:
                return resp
        return resp

    finally:
        request.app['sockets'].remove(resp)
        print('Someone disconnected.')
        for ws in request.app['sockets']:
            await ws.send_str('Someone disconnected.')


async def on_shutdown(app):
    for ws in app['sockets']:
        await ws.close()


async def init(loop):
    app = Application()
    app['sockets'] = []
    app.router.add_get('/', wshandler)
    app.on_shutdown.append(on_shutdown)
    return app


loop = asyncio.get_event_loop()
app = loop.run_until_complete(init(loop))
run_app(app)
