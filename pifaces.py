from __future__ import print_function
from imutils.video import VideoStream
from imutils import face_utils

import numpy as np
import argparse
import imutils
import time
import cv2
import sys, os

ap = argparse.ArgumentParser("")
ap.add_argument("--limit", type=int)
ap.add_argument("--scaleFactor", type=float, default=None, help="scaleFactor, float, default: None (1.1)")
ap.add_argument("--minNeighbors", type=int, default=None, help="minNeighbors, int, default: None (3)")
ap.add_argument("--minSize", type=int, default=None, help="minSize, int, default: None")
ap.add_argument("--cascade", default="./haarcascades/haarcascade_frontalface_default.xml", help="location of the cascade XML file to use, default: ./haarcascades/haarcascade_frontalface_default.xml")
ap.add_argument("--writeFrames", default=False, action="store_true")
args = ap.parse_args()

print ("Loading classifier {0}".format(args.cascade), file=sys.stderr)
detector = cv2.CascadeClassifier(args.cascade)
count = 0
color=(255,0,0)

# vs = VideoStream(src=0).start()
vs = VideoStream(usePiCamera=True).start()
time.sleep(1.0)
facecount = 1

# report/skip/preserve previously save faces
while True:
    outname = "face{0:04d}.jpg".format(facecount)
    if not os.path.exists(outname):
        break
    print ("face", outname)
    facecount += 1


while True:
    frame = vs.read()
    frame = imutils.resize(frame, width=450)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    rects = detector.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

    print ("detector results:{0}".format(len(rects)), file=sys.stderr)
    # facecount = 0
    for (x,y,w,h) in rects:
        # print ((x, y, w, h))
        cv2.rectangle(frame, (x,y), (x+w,y+h), color, 1)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = frame[y:y+h, x:x+w]
        outname = "face{0:04d}.jpg".format(facecount)
        facecount += 1
        cv2.imwrite(outname, roi_gray)
        print ("face", outname)

    if args.writeFrames:
        outname = "frame{0:04d}.jpg".format(count)
        cv2.imwrite(outname, frame)
        print ("frame", outname)

    count += 1
    if args.limit and count >= args.limit:
        break
